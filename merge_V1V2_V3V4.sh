


files=`ls | awk '{gsub("_16S_V1V2.fasta", "", $0); gsub("_16S_V3V4.fasta", "", $0);  print}' | sort | uniq`

for file in $files;
do
    echo $file
    file2=`echo $file | awk '{gsub("-",".",$0); gsub("_",".",$0); print}'`
    cat $file"/16S_V1V2.fasta" $file"/16S_V3V4.fasta" > ../merged/$file.fasta
done
