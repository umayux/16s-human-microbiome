
# Pick Reference OTUs command 
pick_otus.py -i /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//combined/combined_seqs.fna -o /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step1_otus -r /Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/qiime_default_reference/gg_13_8_otus/rep_set/97_otus.fasta -m uclust_ref  --suppress_new_clusters


# Generate full failures fasta file command 
filter_fasta.py -f /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//combined/combined_seqs.fna -s /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step1_otus/combined_seqs_failures.txt -o /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step1_otus/failures.fasta


# Pick rep set command 
pick_rep_set.py -i /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step1_otus/combined_seqs_otus.txt -o /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step1_otus/step1_rep_set.fna -f /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//combined/combined_seqs.fna


# Subsample the failures fasta file using API 
python -c "import qiime; qiime.util.subsample_fasta('/Users/gustavoarango/Documents/projects/XIAO_JAN_DATA/annotation/step1_otus/failures.fasta', '/Users/gustavoarango/Documents/projects/XIAO_JAN_DATA/annotation/step2_otus/subsampled_failures.fasta', '0.100000')

"Forcing --suppress_new_clusters as this is reference-based OTU picking.

# Pick de novo OTUs for new clusters command 
pick_otus.py -i /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step2_otus//subsampled_failures.fasta -o /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step2_otus/ -m uclust  --denovo_otu_id_prefix New.ReferenceOTU


# Pick representative set for subsampled failures command 
pick_rep_set.py -i /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step2_otus//subsampled_failures_otus.txt -o /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step2_otus//step2_rep_set.fna -f /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step2_otus//subsampled_failures.fasta


# Pick reference OTUs using de novo rep set command 
pick_otus.py -i /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step1_otus/failures.fasta -o /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step3_otus/ -r /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step2_otus//step2_rep_set.fna -m uclust_ref  --suppress_new_clusters

# Create fasta file of step3 failures command 
filter_fasta.py -f /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step1_otus/failures.fasta -s /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step3_otus//failures_failures.txt -o /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step3_otus//failures_failures.fasta


# Pick de novo OTUs on step3 failures command 
pick_otus.py -i /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step3_otus//failures_failures.fasta -o /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step4_otus/ -m uclust  --denovo_otu_id_prefix New.CleanUp.ReferenceOTU


# Merge OTU maps command 
cat /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step1_otus/combined_seqs_otus.txt /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step3_otus//failures_otus.txt /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step4_otus//failures_failures_otus.txt > /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//final_otu_map.txt

# Pick representative set for subsampled failures command 
pick_rep_set.py -i /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step4_otus//failures_failures_otus.txt -o /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step4_otus//step4_rep_set.fna -f /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//step3_otus//failures_failures.fasta



# Filter singletons from the otu map using API 
python -c "import qiime; qiime.filter.filter_otus_from_otu_map('/Users/gustavoarango/Documents/projects/XIAO_JAN_DATA/annotation/final_otu_map.txt', '/Users/gustavoarango/Documents/projects/XIAO_JAN_DATA/annotation/final_otu_map_mc2.txt', '2')"

# Write non-singleton otus representative sequences from step1 to the final rep set file: /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//rep_set.fna

# Copy the full input refseqs file to the new refseq file
cp /Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/qiime_default_reference/gg_13_8_otus/rep_set/97_otus.fasta /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//new_refseqs.fna

# Write non-singleton otus representative sequences from step 2 and step 4 to the final representative set and the new reference set (/Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//rep_set.fna and /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//new_refseqs.fna respectively)

Executing commands.

# Make the otu table command 
make_otu_table.py -i /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//final_otu_map_mc2.txt -o /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//otu_table_mc2.biom

Stdout:

Stderr:

Executing commands.

# Assign taxonomy command 


Stdout:

Stderr:

Executing commands.

# Align sequences command 
align_seqs.py -i /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//rep_set.fna -o /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//pynast_aligned_seqs 

Stdout:

Stderr:

# Filter alignment command 
filter_alignment.py -o /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//pynast_aligned_seqs -i /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//pynast_aligned_seqs/rep_set_aligned.fasta 

Stdout:

Stderr:

# Build phylogenetic tree command 
make_phylogeny.py -i /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//pynast_aligned_seqs/rep_set_aligned_pfiltered.fasta -o /Users/gustavoarango/Documents/projects/XIAO_JAN_DATA//annotation//rep_set.tre 

Stdout:

Stderr:

Executing commands.