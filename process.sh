
# # convert fastq to fasta
# files=`ls $PWD`;

# for file in $files;

rootdir="/Users/gustavoarango/Dropbox/Projects/Microbiome/CRAMP_VS_WT_MICE";
#metadata="g3_g4_BAL.txt";

metadata=$1

# combine fasta files
add_qiime_labels.py -m $rootdir/metadata/$metadata -i $rootdir/merged/ -c FileName -o $rootdir/combined/$metadata

# run pick otus
pick_open_reference_otus.py -i $rootdir/combined/$metadata/combined_seqs.fna -o $rootdir/annotation/$metadata/ -s 0.01 -f --suppress_taxonomy_assignment  
# -r /Users/gustavoarango/Documents/projects/taxonomy/RDP/RDP.md52seq.fasta 

### BEGIN MODIFIED ANNOTATION
assign_taxonomy.py -o $rootdir/annotation/$metadata/rdp_assigned_taxonomy -i $rootdir/annotation/$metadata/rep_set.fna -m rdp

# Add taxa to OTU table command 
biom add-metadata -i $rootdir/annotation/$metadata/otu_table_mc2.biom --observation-metadata-fp $rootdir/annotation/$metadata/rdp_assigned_taxonomy/rep_set_tax_assignments.txt -o $rootdir/annotation/$metadata/otu_table_mc2_w_tax.biom --sc-separated taxonomy --observation-header OTUID,taxonomy

###### END

# get summary, important get the counts for the rarefaction. It uses the smallest number to compute the normaliation. 
biom summarize-table -i $rootdir/annotation/$metadata/otu_table_mc2_w_tax.biom -o $rootdir/annotation/$metadata/otu_table_summary.txt

# # single rarefaction  in this case 1994 is the smallest number of reads, so all the samples are normalized to that number
# single_rarefaction.py -i otu_table_mc2_w_tax.biom -o otu_table_rarefied.biom -d 5462 

# normalize with deseq
normalize_table.py -i $rootdir/annotation/$metadata/otu_table_mc2_w_tax.biom --out_path $rootdir/annotation/$metadata/otu_table_mc2_w_tax.deseq2.biom -a DESeq2 -z

biom add-metadata -i $rootdir/annotation/$metadata/otu_table_mc2_w_tax.deseq2.biom -o $rootdir/annotation/$metadata/otu_table_mc2_w_tax.deseq2.tax.biom --observation-metadata-fp $rootdir/annotation/$metadata/rdp_assigned_taxonomy/rep_set_tax_assignments.txt --observation-header OTUID,taxonomy --sc-separated taxonomy

summarize_taxa.py -i $rootdir/annotation/$metadata/otu_table_mc2_w_tax.deseq2.tax.biom  -o $rootdir/annotation/$metadata/tax_levels/ -a

multiple_rarefactions.py -i $rootdir/annotation/$metadata/otu_table_mc2_w_tax.deseq2.tax.biom -m 20 -x 8000 -s 100 -n 10 -o $rootdir/annotation/$metadata/rare/

# alpha diversity

alpha_diversity.py -i $rootdir/annotation/$metadata/rare/ -o $rootdir/annotation/$metadata/alpha_rare/ -t $rootdir/annotation/$metadata/rep_set.tre -m observed_species,chao1,PD_whole_tree

collate_alpha.py -i $rootdir/annotation/$metadata/alpha_rare/ -o $rootdir/annotation/$metadata/alpha_collated/

make_rarefaction_plots.py -i $rootdir/annotation/$metadata/alpha_collated/ -m $rootdir/metadata/$metadata -g pdf -o $rootdir/annotation/$metadata/rarefaction_curves_alpha/

# heatmaps

# First remove the long tags of the taxonomy, only leave the long tags for the Other categories.

python ~/Documents/projects/BioFilm/scripts/remove_taxo_tags.py $rootdir/annotation/$metadata/tax_levels/otu_table_mc2_w_tax.deseq2.tax_L2.txt
python ~/Documents/projects/BioFilm/scripts/remove_taxo_tags.py $rootdir/annotation/$metadata/tax_levels/otu_table_mc2_w_tax.deseq2.tax_L3.txt

Rscript ~/Documents/projects/BioFilm/scripts/heatmap.R $rootdir/annotation/$metadata/tax_levels/otu_table_mc2_w_tax.deseq2.tax_L2.txt.taxo
Rscript ~/Documents/projects/BioFilm/scripts/heatmap.R $rootdir/annotation/$metadata/tax_levels/otu_table_mc2_w_tax.deseq2.tax_L3.txt.taxo

plot_taxa_summary.py -i $rootdir/annotation/$metadata/tax_levels/otu_table_mc2_w_tax.deseq2.tax_L2.txt -l phylum -c pie,bar,area -o $rootdir/annotation/$metadata/tax_levels/phylum_charts/

plot_taxa_summary.py -i $rootdir/annotation/$metadata/tax_levels/otu_table_mc2_w_tax.deseq2.tax_L3.txt -l phylum -c pie,bar,area -o $rootdir/annotation/$metadata/tax_levels/class_charts/

make_otu_heatmap.py -i $rootdir/annotation/$metadata/tax_levels/otu_table_mc2_w_tax.deseq2.tax_L2.biom -o $rootdir/annotation/$metadata/tax_levels/phylum.pdf --no_log_transform --height 5 --width 4

make_otu_heatmap.py -i $rootdir/annotation/$metadata/tax_levels/otu_table_mc2_w_tax.deseq2.tax_L3.biom -o $rootdir/annotation/$metadata/tax_levels/class.pdf --no_log_transform --height 8 --width 4

cp ~/Documents/projects/BioFilm/scripts/make_report.html $rootdir/annotation/$metadata/index.html
cp ~/Documents/projects/BioFilm/scripts/bootstrap.css $rootdir/annotation/$metadata/
