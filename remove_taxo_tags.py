import sys
import numpy as np  

fi = [i.strip().split("\t") for i in open(sys.argv[1])]
fo = open(sys.argv[1]+".taxo","w");

relab = {};
header = []

for i in fi:
    if "#OTU" in i[0]: 
        fo.write("\t".join([i[0].replace(" ","_")]+i[1:]).replace("#","")+"\n");
        continue 
    name = i[0].split(";")[-1].split("__")[-1];
    if name == "Other" or name == "":
        name = "Other";
    
    try:
        relab[name] += np.array([float(k) for k in i[1:]])
    except:
        relab[name] = np.array([float(k) for k in i[1:]])

for i in relab:
    fo.write("\t".join([i]+[str(k) for k in relab[i]])+"\n");


